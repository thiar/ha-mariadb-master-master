# README #

## Instalasi mariadb galera cluster ##
* Install software property

```
#!python

sudo apt-get install software-properties-common 
```

* Tambahkan key ke apt sudo 
```
#!python

apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xcbcb082a1bb943db
```
* Tambahkan repositori ke apt

```
#!python

sudo add-apt-repository 'deb http://ftp.osuosl.org/pub/mariadb/repo/10.0/ubuntu trusty main'
```

* update repositori apt
```
#!python

sudo apt-get update
```
* install mariadb galera server
```
#!python

sudo apt-get install mariadb-galera-server

```
link [Instalasi mariadb galera server](https://mariadb.com/kb/en/mariadb/installing-mariadb-deb-files/)

## Menjalakan replikasi master-master ##

* Konfigurasi galera pada server. Buat file baru dengan perintah
```
#!python

sudo nano /etc/mysql/conf.d/cluster.cnf
```
Tambahkan baris berikut
```
#!python

[mysqld]
query_cache_size=0
binlog_format=ROW
default-storage-engine=innodb
innodb_autoinc_lock_mode=2
query_cache_type=0
bind-address=0.0.0.0

# Galera Provider Configuration
wsrep_provider=/usr/lib/galera/libgalera_smm.so
#wsrep_provider_options="gcache.size=32G"

# Galera Cluster Configuration
wsrep_cluster_name="test_cluster"
wsrep_cluster_address="gcomm://first_ip,second_ip,third_ip"

# Galera Synchronization Congifuration
wsrep_sst_method=rsync
#wsrep_sst_auth=user:pass

# Galera Node Configuration
wsrep_node_address="this_node_ip"
wsrep_node_name="this_node_name"
```
Untuk melakukan tuning lebih lanjut bisa mengunjungi [galera parameter](http://galeracluster.com/documentation-webpages/?id=galera_parameters)

* Menjalankan cluster. Pada node pertama, buatlah cluster dengan menjalankan perintah 
```
#!python

sudo service mysql bootstrap
```
* Ubah konfigurasi user maintenance pada debian dengan menyamakan file /etc/mysql/debian.cnf pada setiap node dengan node1

Kemudian pada node-node yang akan bergabung kepada cluster yang sama pada node pertama, jalankan perintah
```
#!python

sudo service mysql start --wsrep_cluster_address=gcomm://"ip node pertama"

```

* Fail over
[link skenario node fail](https://www.percona.com/blog/2014/09/01/galera-replication-how-to-recover-a-pxc-cluster/)
* faq

1. [solusi untuk sesi anda telah berakhir](http://galeracluster.com/2015/06/achieving-read-after-write-semantics-with-galera/)


## Load Balancing ##

### HAProxy ###
* Tambahkan user haproxy pada mysql, haproxy melakukan pengecekan apakah node mati atau tidak

```
#!python

create user 'haproxy'@'10.151.38.151';flush privileges;
```

* Konfigurasi HAProxy
```
#!python

# this config needs haproxy-1.4.20

global
        log 127.0.0.1   local0
        log 127.0.0.1   local1 notice
        maxconn 4096
        uid 99
        gid 99
        daemon
        #debug
        #quiet

defaults
        log     global
        mode    http
        option  tcplog
        option  dontlognull
        retries 3
        option redispatch
        maxconn 2000
        contimeout      5000
        clitimeout      50000
        srvtimeout      50000

listen mysql-cluster 0.0.0.0:3306
    mode tcp
    balance leastconn
    option mysql-check user haproxy
    log global
    server db01 10.151.38.149:3306 check
    server db02 10.151.38.150:3306 check


```